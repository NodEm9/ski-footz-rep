# Ski-Footz-rep 

## An Ecommerce shopping application
*This* app is a fullstack application build on the below mentioned following Technologies.

- [Vue.js](https://vuejs.org/) , 
- [Express](https://expressjs.com/) , 
- [Node.js](https://nodejs.org/en/) , 
- [MongoDB](https://www.mongodb.com/) and
- [heroku](https://www.heroku.com/home)

The Ecommerce app displays fake-data which was manuelly populated into the `mongodb shell` during development and at when configurating for production release. 

# Building Front End
 The frontend was built with Vue.js

*T*he app Adds to cart, delete from cart, it also views products details and there is computed property to calculate totalPrice of products in cart. With various [Vue.js derectives](https://vuejs.org/v2/api/#Vue-directive) `decoupled` pages for `scalability, simplicity, readablity and realibity` for reuse purposes where binded.
 
# Building Back End
The backend was build with the following Technologies:
- [Node.js](https://nodejs.org/en/) and
- [Express](https://expressjs.com/) 

### DataBase
While for the database [MongoDB](https://www.mongodb.com/) was use to persist our data more permanetly. and

### Deployment
[Heroku](https://www.heroku.com/home) was use to deploy and publish the git respository online.




